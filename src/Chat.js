import React, {Component} from "react";
import io from "socket.io-client";
class Chat extends Component{
    constructor(props){
        super(props);
        var self = this;
        this.state = {
            username: '',
            message: '',
            messages: [],
            allMessages: [],
            page: 0,
            perPage: 10,
            limit: 0,
            removePass: '',
        };

        //this.socket = io('https://utakata-umigame3-pb10001.c9users.io/');
        this.socket = io('localhost:5000');
        this.socket.on('connect', function(){
          console.log('connected');
          self.socket.emit('join', 'LobbyChat');
          self.socket.emit('fetchLobby');
          //self.socket.emit('REQUEST_MESSAGES');
        });

        this.socket.on('lobbyChat', function(msgs){
          self.setState({allMessages: msgs});
          paginate(self.state.page);
        });

        const addMessage = data => {
            console.log(data);
            this.setState({allMessages: [...this.state.allMessages, data]});
            console.log(this.state.messages);
            paginate();
        };

        const paginate = (page) => {
          var msgs = [];
          for (var i = 0; i < this.state.perPage; i++) {
            if(this.state.allMessages[this.state.perPage*(page) + i])
              msgs.push(this.state.allMessages[this.state.perPage*(page) + i]);
          }
          this.setState({messages: msgs});
        };

        this.sendMessage = ev => {
            ev.preventDefault();
            let data = {
              type: 'lobbyChat',
              name: this.state.username,
              content: this.state.message,
              removePass: this.state.removePass
            };
            this.socket.emit('message', data)
            this.setState({limit: this.state.limit+1});
            this.setState({message: ''});
            paginate(this.state.page);
        }
        this.nextPage = () => {
          this.setState({page: this.state.page+1});
          paginate(this.state.page+1);
        }
        this.prevPage = () => {
          this.setState({page: this.state.page-1});
          paginate(this.state.page-1);
        }
        this.remove = () => {
          this.socket.emit('removeAll');
        };
    }
    render(){
        return (
            <div className="container">
              <h1 style={{textAlign: "center"}}>ロビー</h1>
              <div className="control-row input-append">
                  <input type="text" placeholder="Username" value={this.state.username} onChange={ev => this.setState({username: ev.target.value})} className="form-control"/>
                  <br/>
                  <input type="text" placeholder="Removepass" value={this.state.removePass} onChange={ev => this.setState({removePass: ev.target.value})} className="form-control"/>
                  <br/>
                  <input type="text" placeholder="Message" className="form-control" value={this.state.message} onChange={ev => this.setState({message: ev.target.value})}/>
                  <br/>
                  <button onClick={this.sendMessage} className="btn btn-default">Send</button>
              </div>
              <div className="row">
                <ul className="pagination" style={{display: "table", margin: "0 auto"}}>
                  <li><span>ページ: {this.state.page}</span></li>
                  <li><button className="btn" onClick={this.prevPage}>Prev</button></li>
                  <li><button className="btn" onClick={this.nextPage}>Next</button></li>
                </ul>
              </div>
                <div className="row">
                  <div className="chat-area">
                          {this.state.messages.map(message => {
                              return (
                                <div key={message.id} className="chat-row">
                                  <div style={{width: "80%", margin: "0 auto"}} className="chat-box">
                                    <small className="secondary-text">{message.name}</small>
                                    <span className="primary-text message-text">{message.content}</span>
                                  </div>
                                </div>
                              )
                          })}
                  </div>
                </div>
                <button onClick={this.remove} className="btn btn-default">Remove all</button>
              </div>
        );
    }
}

export default Chat;
